package com.devcamp.countryregion;


// tỉnh 
public class Region {

    private String regionCode;
    private String regionName;

    // consttructer

    public Region(String regionCode, String regionName) {
        this.regionCode = regionCode;
        this.regionName = regionName;
    }
    public String getRegionCode() {
        return regionCode;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    public String getRegionName() {
        return regionName;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    @Override
    public String toString() {
        return "Region [regionCode=" + regionCode + ", regionName=" + regionName + "]";
    }

    

}
