package com.devcamp.countryregion;

import java.util.ArrayList;

public class Country {
    private String countryCode;  // mã quốc gia
    private String countryName;  // tên quốc gia
    private  ArrayList <Region> regions;  // vùng đất   

    // construster
    public Country(String countryCode, String countryName, ArrayList<Region> regions) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regions = regions;
    }

    

    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }



    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }



    @Override
    public String toString() {
        return "Country [countryCode=" + countryCode + ", countryName=" + countryName + ", regions=" + regions + "]";
    }

    
    


    

}
