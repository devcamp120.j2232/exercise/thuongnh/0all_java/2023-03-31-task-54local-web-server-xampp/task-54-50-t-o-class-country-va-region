package com.devcamp.countryregion;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryRegionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryRegionApplication.class, args);

		

		// khởi tạo các tỉnh ở việt nam 
		Region haNoi = new Region("HA NOi", "Hà Nội");
		Region daNang = new Region("DA NANG", "đà nẵng");
		Region thanhHoa = new Region("THANH HOA", "Thanh Hóa");
		// thêm vào 1 arr list  arr viet nam
		ArrayList<Region> vietNamList = new ArrayList<Region>();
		vietNamList.add(haNoi);
		vietNamList.add(daNang);
		vietNamList.add(thanhHoa);

		// Khởi tạo danh sách country và in ra console các country. Trong đó có Việt nam

		Country countryMy = new Country("USE", "MỸ ");
		Country countryChiNa = new Country("TQ", "Trung của ");
		Country countryVietNAM = new Country("VIET NAM", "Việt Nam ", vietNamList);

		// thêm 3 quốc gia vào arr list 

		ArrayList<Country> country = new ArrayList<Country>();
		country.add(countryMy);
		country.add(countryChiNa);
		country.add(countryVietNAM);

		// Duyệt danh sách country đã có, nếu là việt nam thì in danh sách các regions(tỉnh/tp) ra console 
		for (Country countrys : country ){
			//System.out.println(countrys);
			if(countrys.getCountryCode().equals("VIET NAM")){
				// in danh sách các tỉnh 
				for(Region  cacTinhs:  vietNamList){
					System.out.println(cacTinhs);
				}

			}
		}


		// System.out.println(countryMy);
		// System.out.println(countryChiNa);
		// System.out.println(countryVietNAM);



	}

	

}
